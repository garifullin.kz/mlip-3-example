import random
import numpy as np
import sys
import subprocess
N = int(sys.argv[1])
atom_type=1
x = random.uniform(0.0, 39.57)
y = random.uniform(0.0, 38.07)
coord = str(N) + " " + str(atom_type) + " " + str(x)+" "+str(y)+" "+str(38) + '\n'
name = "Cu_" + str(N) +".data"
test=open(name, 'a')
test.write(coord)
test.close()
with open ('in.my', 'r') as f:
    old_data = f.read()
str_new='group           top id 1081:'+str(N-1)
str_old='group           top id 1081:'+str(N-2)
new_data = old_data.replace(str_old, str_new)
str_new='group           addatom id '+str(N)
str_old='group           addatom id '+str(N-1)
new_data = new_data.replace(str_old, str_new)
str_old='read_data       Cu_' + str(N-1) + '.data'
str_new='read_data       Cu_' + str(N)   + '.data'
new_data = new_data.replace(str_old, str_new)
with open ('in.my', 'w') as f:
    f.write(new_data)
